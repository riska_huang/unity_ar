﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;

public class PinchZoom : MonoBehaviour
{
    [SerializeField]
    private float perspectiveZoomSpeed = 0.5f;

    [SerializeField]
    private float orthoZoomSpeed = 0.5f;

    void Update()
    {
        PinchToZoom();
    }

    private void PinchToZoom()
    {
        if (Input.touchCount == 2)
        {
            //Store both touches
            Touch touchZero = Input.GetTouch(0);
            Touch touchOne = Input.GetTouch(1);

            // Find the position in the previous frame of each touch.
            Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
            Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

            // Find the magnitude of the vector (the distance) between the touches in each frame.
            float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
            float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

            // Find the difference in the distances between each frame.
            float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;

            //zoom the model
            float diff = deltaMagnitudeDiff * orthoZoomSpeed;
            Vector3 logoInitScale = GeneralManager.instance.logoGO.transform.localScale;
            Vector3 logoScale = GeneralManager.instance.logoGO.transform.localScale;

            //To check so that the model will not scaling in the opposite direction
            if (logoScale.x < 12.421 && logoScale.y < 0 && logoScale.z < 12.241)
            {
                GeneralManager.instance.logoGO.transform.localScale = new Vector3(12.242f, 0.1f, 12.242f);
            }
            else
            {
                GeneralManager.instance.logoGO.transform.localScale =
                    new Vector3(logoScale.x -= diff, logoScale.y -= diff, logoScale.z -= diff);
            }


        }

    }
}
