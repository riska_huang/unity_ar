﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwipeAnimate : MonoBehaviour
{

    private void Awake()
    {
        SwipeDetector.OnSwipe += SwipeDetector_OnSwipe;
    }


    //Start animation when swiped
    private void SwipeDetector_OnSwipe(SwipeDetector.SwipeData data)
    {
        Debug.Log("Swipe in Direction: " + data.Direction);
        GeneralManager.instance.StartLogoAnimation();

        //Can add different animation based on swipe direction
        //Haven't implemented it yet. Sorry (_ _)
    }
}
