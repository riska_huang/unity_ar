﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class GeneralManager : MonoBehaviour
{
    #region Variable
    public static GeneralManager instance;
    //Public variable, object
    public GameObject logoGO;
    public Camera camera;
    public GameObject panelShare;

    //private variable, object component
    private Animator animator;
    private ParticleSystem particle;

    //file path
    string filename = "Screenshot" + DateTime.Now.Hour + DateTime.Now.Minute + DateTime.Now.Second + ".png";
    string folderLocation = "sdcard/DCIM/Screenshots/";

#endregion

    void Awake()
    {
        #region Setting up Singleton
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
        DontDestroyOnLoad(gameObject);
        #endregion
    }

    /// <summary>
    /// Initialization.
    /// Initialize all variable
    /// </summary>
    void Start()
    {
        if (logoGO == null)
        {
            logoGO = GameObject.FindWithTag("Logo");
        }

        if (camera == null)
        {
            camera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
        }

        if (animator == null)
        {
            animator = logoGO.GetComponent<Animator>();

        }

        if (particle == null)
        {
            particle = logoGO.transform.GetChild(0).GetComponent<ParticleSystem>();
        }

        //the panel are initially closed
        CloseSharePanel();
    }

    //Screenshot button GUI
    private void OnGUI()
    {
        GUIStyle style = new GUIStyle(GUI.skin.button);
        style.fontSize = 60;

        if (GUI.Button(new Rect(25, 25, 400, 100), "ScreenShot", style))
        {
            SaveScreenShot();
        }
    }

    #region Animation
    public void StopLogoAnimation()
    {
        animator.enabled = false;
    }

    public void StartLogoAnimation()
    {
        StartCoroutine(StartAnimate());
    }

    private IEnumerator StartAnimate()
    {
        animator.enabled = true;
        //Play Animation
        if (animator != null)
        {
            animator.Play("rotateAnime", 0, 0.25f);
            yield return new WaitForSeconds(2);
        }

        animator.enabled = false;
    }

    #endregion

    #region Particle and Sound Effect
    public void PlayParticle()
    {
        particle.Play();
    }

    public void PlaySound()
    {
        logoGO.GetComponent<AudioSource>().Play();
    }
    #endregion

    #region Screenshot and share to social media
    public void SaveScreenShot()
    {
        StartCoroutine(SaveSS());
    }

    private IEnumerator SaveSS()
    {
        yield return new WaitForEndOfFrame();

        string location = folderLocation + filename;

        //Ensure the location is exist
        if (!Directory.Exists(folderLocation))
            Directory.CreateDirectory(folderLocation);

        Texture2D texture = ScreenCapture.CaptureScreenshotAsTexture();

        byte[] bytes = texture.EncodeToPNG();
        Destroy(texture);
        File.WriteAllBytes(location,bytes);

        yield return new WaitForSeconds(1);
        panelShare.SetActive(true);
    }

    public void ShareToSosmed()
    {
        StartCoroutine(ShareSosmed());
    }

    private IEnumerator ShareSosmed()
    {
        yield return new WaitForEndOfFrame();
        string location = folderLocation + filename;

        new NativeShare().AddFile(location).SetText("Hi, I am sharing my Arutala Logo with you.").Share();
        CloseSharePanel();
    }

    /// <summary>
    /// Panel that store buttons to choose whether to share screenshot or not
    /// </summary>
    public void CloseSharePanel()
    {
        panelShare.SetActive(false);
    }
#endregion
}
