
These files are needed for processing PDF files containing japanese encodings.

Copy them into the pvsdk.dll directory,
or add an entry in the pvsdk.ini file in the pvsdk.dll directory:

        EncodingDir=path_to_encoding_files

Note: these files are now in the Adobe standard CMap format.

...

The ppd files can be used to help decode CJK fonts in ps files.
set:
        PPDFile=filename

as appropriate.


